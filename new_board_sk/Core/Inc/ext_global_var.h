/*
 * global_var.h
 *
 *  Created on: Mar 5, 2020
 *      Author: tarun
 *
 */

//#include "stm32f1xx_hal_iwdg.h"
//#include "stm32f1xx_hal_def.h"
#include "stm32f1xx.h"

#ifndef INC_GLOBAL_VAR_H_
#define INC_GLOBAL_VAR_H_

extern unsigned char Motor_Flag;
extern unsigned char Motor_Speed;
extern unsigned char Motor_Direction;
extern int Motor_Duration;
extern int Motor_Time_Count;

extern IWDG_HandleTypeDef hiwdg;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim3;


#endif /* INC_GLOBAL_VAR_H_ */
