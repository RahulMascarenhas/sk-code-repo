 /*
 * weight_sensor.h
 *
 *  Created on: Mar 4, 2020
 *      Author: tarun
 */

#ifndef INC_WEIGHT_SENSOR_H_
#define INC_WEIGHT_SENSOR_H_

#include "stm32f1xx_hal.h"
#include "math.h"

#define DATA_PORT					GPIOA
#define CLOCK_PORT					GPIOA

#define DATA_PIN					GPIO_PIN_0
#define CLOCK_PIN					GPIO_PIN_1

#define CLOCK_LINE_LOW()			HAL_GPIO_WritePin(CLOCK_PORT, CLOCK_PIN, 0)
#define CLOCK_LINE_HIGH()			HAL_GPIO_WritePin(CLOCK_PORT, CLOCK_PIN, 1)

#define DATA_LINE_READ()			HAL_GPIO_ReadPin(DATA_PORT, DATA_PIN)


#define CH_A_128_GAIN_PULSES		25
#define CH_B_32_GAIN_PULSES  		26
#define CH_B_64_GAIN_PULSES  		27

//To calculate the value of the weight without any tare calibration

#define GAIN_FACTOR 		 		128
#define SENSITIVITY_SCALE			0.001//0.002
#define MAX_WEIGHT_SCALE			10//40
#define FULL_LOAD_VOLTAGE_HX711		0.5
#define SENSITIVITY_AG_HX711		(SENSITIVITY_SCALE * GAIN_FACTOR) //AG is After Gain
#define FULL_LOAD_S_VOLTAGE_HX711	(SENSITIVITY_AG_HX711/FULL_LOAD_VOLTAGE_HX711)
//#define FULL_LOAD_S_DECIMAL_HX711	(FULL_LOAD_S_VOLTAGE_HX711 * (2^23))
#define FULL_LOAD_S_DECIMAL_HX711	FULL_LOAD_S_VOLTAGE_HX711 * 8388608 //pow(2, 23)
#define IDEAL_VALUE_PER_KG			(int) (FULL_LOAD_S_DECIMAL_HX711/MAX_WEIGHT_SCALE)


//Function declarations

unsigned long ReadCount(void);
unsigned long get_data();
double raw_decimal_data();
double get_tare(int number_of_readings);
double weight_in_kg(double error);

#endif /* INC_WEIGHT_SENSOR_H_ */
