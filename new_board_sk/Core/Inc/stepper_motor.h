/*
 * stepper_motor.h
 *
 *  Created on: Jun 20, 2020
 *      Author: tarun
 */

#ifndef INC_STEPPER_MOTOR_H_
#define INC_STEPPER_MOTOR_H_
#include "stm32f1xx_hal.h"


#endif /* INC_STEPPER_MOTOR_H_ */

#define DIR_CLOCK_WISE			0
#define DIR_ANTI_CLOCK_WISE		1
#define CONTROL_PORT			GPIOA
#define PIN_1					GPIO_PIN_9
#define PIN_2					GPIO_PIN_11
#define PIN_3					GPIO_PIN_10
#define PIN_4					GPIO_PIN_12



void init_motors();
//void start_motor(unsigned char direction, unsigned char delay);
void start_motor(
		unsigned char direction,
		unsigned char speed,
		unsigned char duration);
void stop_motor();
void test_motor();
void test_new_motor();
