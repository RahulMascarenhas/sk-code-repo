/*
 * instruction_set.h
 *
 *  Created on: Mar 12, 2020
 *      Author: Rahul
 */

#ifndef INC_INSTRUCTION_SET_H_
#define INC_INSTRUCTION_SET_H_

//LED test functions

#define INSTR_BLINK_LED			0x01
#define INSTR_LED_ON			0x02
#define INSTR_LED_OFF			0x03

#endif /* INC_INSTRUCTION_SET_H_ */
