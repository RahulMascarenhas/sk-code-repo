/*
 * sk_peripherals.h
 *
 *  Created on: 28-Apr-2020
 *      Author: Rahul
 */

#ifndef INC_SK_PERIPHERALS_H_
#define INC_SK_PERIPHERALS_H_



#endif /* INC_SK_PERIPHERALS_H_ */

//Types of dispensers
#define PRL_SPICE_DISPENSER		0x01
#define PRL_LIQUID_DISPENSER	0x02
#define PRL_SOLID_DISPENSER		0x03

//List of commands
#define CMD_RECALIB_TARE_VALUE	0x00
#define CMD_WEIGHT_READING		0x01
#define CMD_DISPENSE_ITEM		0x02
#define CMD_CHECK_STATUS		0x03
#define CMD_START_MOTOR			0x04
#define CMD_STOP_MOTOR			0x05
#define CMD_MOTOR_STATUS		0x06

//List of functions
/*
void process_i2c_instruction(unsigned char rx_array[], double tare_error);
void dispense_item(
		double tare_error,
		unsigned char gms_to_dispense
		);
*/


//List of ingredients

enum POWDERED_SPICE{
	SALT,
	PEPPER,
	GARAM_MASALA,
	CUMIN_POWDER,
	TUMERIC_POWDER,
	CORIANDER_POWDER,	//Dhaniya powder
	CINNAMON_POWDER,
	AMCHOOR_POWDER
};

enum WHOLE_SPICE{
	CUMIN_SEEDS,
	CORIANDER_SEEDS,
	MUSTARD_SEEDS,		//Rai
	CINNAMON_STICK,
	CLOVE,				//Laung
	CARDAMOM_PODS,		//Elaichi
	BAY_LEAF
};
