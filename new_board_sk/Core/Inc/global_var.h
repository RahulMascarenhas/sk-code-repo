/*
 * global_var.h
 *
 *  Created on: Mar 5, 2020
 *      Author: tarun
 *
 */

//#include "stm32f1xx_hal_iwdg.h"
//#include "stm32f1xx_hal_def.h"
#include "stm32f1xx.h"

#ifndef INC_GLOBAL_VAR_H_
#define INC_GLOBAL_VAR_H_

unsigned char Motor_Flag;
unsigned char Motor_Speed;
unsigned char Motor_Direction;
int Motor_Duration;
int Motor_Time_Count;

IWDG_HandleTypeDef hiwdg;
TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim3;


#endif /* INC_GLOBAL_VAR_H_ */
