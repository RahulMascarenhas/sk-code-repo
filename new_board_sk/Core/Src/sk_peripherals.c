/*
 * sk_peripherals.c
 *
 *  Created on: 28-Apr-2020
 *      Author: Rahul
 */
#include "sk_peripherals.h"
//#include "stm32f0xx_hal_i2c.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_tim.h"
#include "stm32f1xx_hal_def.h"

/*


void process_i2c_instruction(unsigned char rx_array[], double tare_error){
	if(rx_array[0] == CMD_WEIGHT_READING){
		//Respond with weight value
		i2c_respond(CMD_WEIGHT_READING, weight_in_kg(tare_error));
	}
	else if(rx_array[0] == CMD_DISPENSE_ITEM){
		dispense_item(tare_error, rx_array[1]);
		i2c_respond(CMD_DISPENSE_ITEM, 200);
	}
}

void i2c_respond(I2C_HandleTypeDef *hi2c, unsigned char cmd, unsigned char data){
	unsigned char response[2];
	response[0] = cmd;
	response[1] = data;
	HAL_I2C_Slave_Transmit_IT(&hi2c, response, 2);
}

void dispense_item(
		double tare_error,//TODO replace with flash memory storage instead of passing this
		unsigned char gms_to_dispense
		){
	//This function will dispense the item required
	unsigned char i;
	double starting_weight, difference;
	starting_weight = difference = weight_in_kg(tare_error);

	//while((staring_weight - weight_in_kg(tare_error)) >= (gms_to_dispense*1000)){
	while(difference <= (gms_to_dispense*1000)){
		difference = starting_weight - weight_in_kg(tare_error);

		//Lower the difference between the starting and current weight, higher the PWM speed
		if(difference > 0.02){
			i = 65;
		}
		else if(difference > 0.01){
			i = 125;
		}
		else{
			i = 250; //Full speed PWM
		}
		//htim14.Instance->CCR1 = i;
		timer.Instance->CCR1 = i;
	}

}

*/
