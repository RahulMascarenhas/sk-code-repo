/*
 * stepper_motor.c
 *
 *  Created on: Jun 20, 2020
 *      Author: Rahul
 */

#include "stepper_motor.h"
#include "ext_global_var.h"


const int PINS[] =  {GPIO_PIN_9, GPIO_PIN_11, GPIO_PIN_10, GPIO_PIN_12};


void init_motors(){
	/*
	HAL_GPIO_WritePin(CONTROL_PORT, PIN_1, 0);
	HAL_GPIO_WritePin(CONTROL_PORT, PIN_2, 0);
	HAL_GPIO_WritePin(CONTROL_PORT, PIN_3, 0);
	HAL_GPIO_WritePin(CONTROL_PORT, PIN_4, 0);
	*/
	HAL_GPIO_WritePin(CONTROL_PORT, GPIO_PIN_10, 0);
	//Motor_Flag = Motor_Duration = Motor_Time_Count = 0;
	//HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
	//HAL_TIM_Base_Stop_IT(&htim3);
}

void start_motor(
		unsigned char direction,
		unsigned char speed,
		unsigned char duration){

/*
 * This code is for controlling a stepper with L298
 	unsigned char i = 0;
	if(direction == DIR_CLOCK_WISE){
		for(i = 0; i<4;i++){
			HAL_GPIO_WritePin(CONTROL_PORT, PINS[i], 1);
			HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
			HAL_Delay(speed);
			HAL_GPIO_WritePin(CONTROL_PORT, PINS[i], 0);
		}
	}
	else if(direction == DIR_ANTI_CLOCK_WISE){
		for(i = 4; i>0;i--){
			HAL_GPIO_WritePin(CONTROL_PORT, PINS[i-1], 1);
			HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
			HAL_Delay(speed);
			HAL_GPIO_WritePin(CONTROL_PORT, PINS[i-1], 0);
		}
	}
*/

	/*
	unsigned char steps_per_revolution = 200;

	unsigned char x;

	HAL_GPIO_WritePin(CONTROL_PORT, GPIO_PIN_9, direction); //direction

	for(x = 0; x < steps_per_revolution; x++){
		HAL_GPIO_TogglePin(CONTROL_PORT, GPIO_PIN_10);
		HAL_Delay(speed);
		//HAL_GPIO_WritePin(CONTROL_PORT, GPIO_PIN_10, 0);
		//HAL_Delay(1000);
	}
	*/

	if(Motor_Flag == 0){
		HAL_TIM_Base_Stop_IT(&htim3);
		HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
		//

		Motor_Duration = duration;// * 60;
		HAL_GPIO_WritePin(CONTROL_PORT, GPIO_PIN_9, direction); //direction

		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
		htim1.Instance->CCR1 = 0xA;
		//htim1.Instance->ARR = speed;
		HAL_TIM_Base_Start_IT(&htim3);
	}
}

void stop_motor(){
	init_motors();
}

void test_motor(){
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 1);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, 0);
	HAL_Delay(40);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 1);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, 0);
	HAL_Delay(40);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, 1);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, 0);
	HAL_Delay(40);

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, 0);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_12, 1);
	HAL_Delay(40);


}

void test_new_motor(){
	unsigned char steps_per_revolution = 200;
	int x;

	HAL_GPIO_WritePin(CONTROL_PORT, GPIO_PIN_9, 01); //direction

	for(x = 0; x < steps_per_revolution; x++){

		HAL_GPIO_TogglePin(CONTROL_PORT, GPIO_PIN_10);
		HAL_Delay(5);
		//HAL_GPIO_WritePin(CONTROL_PORT, GPIO_PIN_10, 0);
		//HAL_Delay(1000);
	}
}
