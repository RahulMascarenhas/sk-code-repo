/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "weight_sensor.h"
#include "sk_peripherals.h"
#include "stepper_motor.h"
#include "global_var.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define SLAVE_ADDRESS		(0x10 << 1)
#define TARE_ERROR			0

#define PWM_TIME_PERIOD		10//300		//Spice dispenser
#define PWM_TIME_PERIOD_LD	100//Liquid dispenser

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

extern IWDG_HandleTypeDef hiwdg;

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim3;

/* USER CODE BEGIN PV */

extern unsigned char Motor_Flag;
extern unsigned char Motor_Speed;
extern unsigned char Motor_Direction;

unsigned char Trigger_Flag = 0;
int Process_Flag = 0;
double Tare_Error = TARE_ERROR;
double in_kg = 0;
double in_kg_test = 0;
double error = 0;
unsigned char i = 0;
unsigned char r = 250;
unsigned char temp_flag = 0;

unsigned char temp_array[] = {CMD_DISPENSE_ITEM, 0xA, 0xFF};//{0x71, 0x88, 0x95};
unsigned char temp_tare[] = {CMD_RECALIB_TARE_VALUE, 0x00, 0x00};
unsigned char temp_cmd_rxd[] = {SLAVE_ADDRESS, CMD_DISPENSE_ITEM, 0x00};

unsigned char rx_array[64];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
static void MX_IWDG_Init(void);
static void MX_TIM3_Init(void);
/* USER CODE BEGIN PFP */

void dispense_item1(
		double tare_error,//TODO replace with flash memory storage instead of passing this
		double gms_to_dispense,
		unsigned char speed
		);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void dispense_item1(double tare, double gms_to_dispense, unsigned char speed){
	double starting_weight, check_weight;
	unsigned char pwm_speed, j;


	starting_weight = 0;
	for(j=0;j<3;j++){
		starting_weight += weight_in_kg(tare);
	}
	starting_weight = starting_weight/3;

	//starting_weight = weight_in_kg(tare); //OG code
	check_weight = 0;

	gms_to_dispense = gms_to_dispense/1000; //convert from gms to kg

	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);

	while((starting_weight - weight_in_kg(tare)) <= gms_to_dispense){
		//htim14.Instance->CCR1 = 0;
		//HAL_Delay(50);
		//HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1);
		//check_weight = weight_in_kg(tare);//OGG code this works
		//HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
		//htim14.Instance->CCR1 = 190;//OG code
		//htim14.Instance->CCR1 = speed; //OGG code this works
		if(gms_to_dispense - (starting_weight - weight_in_kg(tare)) <= 0.004){
			//Slow the dispensing down for the last 4gms
			htim1.Instance->CCR1 = 100;
		}
		else{
			htim1.Instance->CCR1 = speed;
		}
	}


	htim1.Instance->CCR1 = 0;

	HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
}

void process_i2c_instruction(unsigned char rx_array[], double tare_error){
	if(rx_array[0] == CMD_WEIGHT_READING){
		//Respond with weight value
		//i2c_respond(CMD_WEIGHT_READING, weight_in_kg(tare_error));
	}
	else if(rx_array[0] == CMD_DISPENSE_ITEM){
		//i2c_respond(CMD_DISPENSE_ITEM, 200);
		//dispense_item1(tare_error, rx_array[1], rx_array[2]);//OG
		dispense_item1(Tare_Error, rx_array[1], rx_array[2]);
	}
	else if(rx_array[0] == CMD_RECALIB_TARE_VALUE){
		Tare_Error = get_tare(20);
	}
	else if(rx_array[0] == CMD_START_MOTOR){
		/*
		//start_motor(rx_array[1],rx_array[2]);
		Motor_Direction = rx_array[1];
		Motor_Speed = rx_array[2];
		Motor_Flag = 1;
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, rx_array[1]);
		HAL_TIM_Base_Start_IT(&htim3);
		htim3.Instance->ARR = rx_array[2];
		*/

		start_motor(rx_array[1], 10, rx_array[2]);
		Motor_Flag = 1;
		//hi2c1.State = HAL_I2C_STATE_RESET;
	}
	else if(rx_array[0] == CMD_STOP_MOTOR){
		stop_motor();
		Motor_Flag = 0;
		//HAL_TIM_Base_Stop_IT(&htim3);
	}
	HAL_IWDG_Refresh(&hiwdg);
}



/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
     HAL_Init();

  /* USER CODE BEGIN Init */
  init_motors();
  //test_motor();
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_IWDG_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
  //error = get_tare(20);
  //HAL_TIM_Base_Start_IT(&htim3);
  //htim3.Instance->ARR = 5;
  /*
  HAL_TIM_Base_Stop_IT(&htim3);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  htim1.Instance->CCR1 = 10;
  //htim1.Instance->ARR = 500;
  */

  /* USER CODE END 2 */
 
 

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */


	  //HAL_IWDG_Refresh(&hiwdg); //remove when i2c is running
	  if(temp_flag){
		  start_motor(0,10,1);
		  temp_flag = 0;
	  }

	  //test_motor();
	  //test_new_motor();

	   //SLAVE MODULE STUFF
	  HAL_I2C_Slave_Receive_IT(&hi2c1, rx_array, 3); // For the Slave
	  if(Process_Flag){
		  process_i2c_instruction(rx_array, TARE_ERROR);

		  for(i = 0; i<15;i++){
			  HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
			  HAL_Delay(100);
			  //HAL_IWDG_Refresh(&hiwdg);
		  }

		  rx_array[0]=rx_array[1]=rx_array[2] = Process_Flag = 0;
		  //hi2c1.State = HAL_I2C_STATE_RESET;
		  //SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);
	  }
/*
	  if(Motor_Flag){
		  start_motor(Motor_Direction, Motor_Speed);
		  HAL_IWDG_Refresh(&hiwdg);
		  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);

	  }
	  else{
		  stop_motor();
	  }
*/

	 //in_kg = weight_in_kg(error);
	 //in_kg_test =  weight_in_kg(TARE_ERROR);
	 //temp_weight = get_data();

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 10000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = SLAVE_ADDRESS;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */
  //hi2c1.Init.OwnAddress1 = SLAVE_ADDRESS;
  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_32;//64;//IWDG_PRESCALER_256;
  hiwdg.Init.Reload = 4095;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 32000;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = PWM_TIME_PERIOD;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 500;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 32000;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1000;//hereee
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_IC_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |GPIO_PIN_12, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA9 PA10 PA11 
                           PA12 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c){
	HAL_IWDG_Refresh(&hiwdg);
	if(hi2c->Instance == I2C1){
		if(rx_array[0] != 0x00){
			if(rx_array[0] == CMD_CHECK_STATUS){
				temp_cmd_rxd[0] = SLAVE_ADDRESS;
				temp_cmd_rxd[1] = CMD_DISPENSE_ITEM;
				temp_cmd_rxd[2] = Process_Flag;	//0: Done or free, 1: Busy
				HAL_I2C_Slave_Transmit_IT(&hi2c1, temp_cmd_rxd, 3);//works here
				rx_array[0] = 0;
			}
			else if(rx_array[0] == CMD_MOTOR_STATUS){
				temp_cmd_rxd[0] = SLAVE_ADDRESS;
				temp_cmd_rxd[1] = 0;
				temp_cmd_rxd[2] = Motor_Flag;	//0: Done or free, 1: Busy
				HAL_I2C_Slave_Transmit_IT(&hi2c1, temp_cmd_rxd, 3);//works here
				rx_array[0] = 0;
			}
			else{
				Process_Flag = 1;
			}
		}
		HAL_IWDG_Refresh(&hiwdg);
	}

}



void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hi2c){
	if(hi2c->Instance == I2C1){
		SET_BIT(hi2c1.Instance->CR1, I2C_CR1_STOP);
		//HAL_I2C_Slave_Receive_IT(&hi2c1, rx_array, 3);

	}
}


/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM2 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM2) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
