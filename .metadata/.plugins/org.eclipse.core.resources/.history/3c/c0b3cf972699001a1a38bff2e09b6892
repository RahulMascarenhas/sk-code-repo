/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
//WEIGHT SENSOR connections

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "weight_sensor.h"
#include "global_var.h"
#include "custom_bus.h"
#include "custom_uart.h"
#include "sk_peripherals.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define SLAVE_ADDRESS 	0x01
#define TARE_ERROR		-19961.189355468749

//#define SLAVE_ADDRESS		0x02
//#define TARE_ERROR			-19959.939453125

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim14;

UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;

/* USER CODE BEGIN PV */
unsigned char Trigger_Flag = 0;
int Process_Flag = 0;
int temp = 0;
int data_read = 0;
unsigned long temp_result = 0;
unsigned long final_result = 0;
double in_kg = 0;
double error = 0;

unsigned long i;
unsigned long factor;

int temp_flag = AMCHOOR_POWDER;
//int temp_array[] = {0x71, 0x88, 0x95};
unsigned char temp_array[] = {CMD_DISPENSE_ITEM, 0xA, 0xF0};//{0x71, 0x88, 0x95};
unsigned char temp_output[64];
unsigned char temp_output1[64];
unsigned char temp_attr;
int temp_nob;
unsigned char temp_length, temp_length1;
//int temp_array[] = {0xC3};
unsigned char rx_array[64];

unsigned long temp_weight = 0;

int device_state = STATE_UNADDRESSED;
const unsigned char START_IDENTIFIER[] = {
										'S',
										'K',
										(DEVICE_ADDRESS & 0xFF),
										((DEVICE_ADDRESS >> 8) & 0xFF),
										((DEVICE_ADDRESS >> 16) & 0xFF)
};

struct rx_packet rx_pkt;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM3_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM14_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */
void process_i2c_instruction(unsigned char rx_array[], double tare_error);
void i2c_respond(unsigned char cmd, unsigned char data);
void dispense_item(
		double tare_error,//TODO replace with flash memory storage instead of passing this
		double gms_tok_dispense
		);
void dispense_item1(
		double tare_error,//TODO replace with flash memory storage instead of passing this
		double gms_to_dispense,
		unsigned char speed
		);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */



void dispense_item(
		double tare_error,//TODO replace with flash memory storage instead of passing this
		double gms_to_dispense
		){
	//This function will dispense the item required
	htim14.Instance->CCR1 = 0;
	unsigned char i, j;
	double starting_weight, difference;

	difference = i = j = 0;

	for(j=0;j<5;j++){
			starting_weight += weight_in_kg(tare_error);
	}
	starting_weight = starting_weight/5;
 	difference = starting_weight - weight_in_kg(tare_error);

	//while((staring_weight - weight_in_kg(tare_error)) >= (gms_to_dispense*1000)){
	while(difference <= (gms_to_dispense/1000)){
		difference = starting_weight - weight_in_kg(tare_error);

		//Lower the difference between the starting and current weight, higher the PWM speed
		if(difference > 0.02){
			i = 65;
		}
		else if(difference > 0.01){
			i = 125;
		}
		else{
			i = 250; //Full speed PWM
		}
		//htim14.Instance->CCR1 = i;
		htim14.Instance->CCR1 = i;
		//HAL_Delay(50);
		//htim14.Instance->CCR1 = 0;
	}

	htim14.Instance->CCR1 = 0;

}

void dispense_item1(double tare, double gms_to_dispense, unsigned char speed){
	double starting_weight, check_weight;
	unsigned char pwm_speed, j;

	/*
	starting_weight = 0;
	for(j=0;j<5;j++){
		starting_weight += weight_in_kg(tare);
	}
	starting_weight = starting_weight/5;
	*/
	starting_weight = weight_in_kg(tare);
	check_weight = 0;

	gms_to_dispense = gms_to_dispense/1000; //convert from gms to kg

	HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);

	while((starting_weight - weight_in_kg(tare)) <= gms_to_dispense){
		//htim14.Instance->CCR1 = 0;
		//HAL_Delay(50);
		//HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1);
		check_weight = weight_in_kg(tare);
		//HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
		//htim14.Instance->CCR1 = 190;//OG code
		htim14.Instance->CCR1 = speed;
	}
	htim14.Instance->CCR1 = 0;

	HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1);
}

void process_i2c_instruction(unsigned char rx_array[], double tare_error){
	if(rx_array[0] == CMD_WEIGHT_READING){
		//Respond with weight value
		i2c_respond(CMD_WEIGHT_READING, weight_in_kg(tare_error));
	}
	else if(rx_array[0] == CMD_DISPENSE_ITEM){
		i2c_respond(CMD_DISPENSE_ITEM, 200);
		dispense_item1(tare_error, rx_array[1], rx_array[2]);
	}
}

void i2c_respond(unsigned char cmd, unsigned char data){
	unsigned char response[2];
	response[0] = cmd;
	response[1] = data;
	HAL_I2C_Slave_Transmit_IT(&hi2c1, response, 2);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */

	HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM3_Init();
  MX_I2C1_Init();
  MX_TIM14_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  //HAL_TIM_Base_Start_IT(&htim3);

  	//HAL_TIM_Base_Start_IT(&htim3);
	//temp = ReadCount();
	//HAL_TIM_Base_Stop_IT(&htim3);
	//HAL_Delay(2000);


  //error = get_tare(20); //Compute the error before the program starts TODO flash storage

  //temp_length = form_output_packet1(PT_INSTR, temp_array, temp_output);//Tx portion
  //uart_transmit_8b(&huart1, temp_output, temp_length);//Tx portion
  temp_length = form_output_packet1(PT_INSTR, temp_array, temp_output, 0xFEEDE4);//Tx portion
  temp_length1 = form_output_packet1(PT_INSTR, temp_array, temp_output1, 0xFEEDE5);//Tx portion

  //PWM STUFF
 // HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
  //htim14.Instance->CCR1 = 0;


  //HAL_UART_Receive_IT(&huart1, rx_array, 1); //UART Rx portion
  //HAL_I2C_Slave_Receive_IT(&hi2c1, rx_array, 1);//I2C Rx portion
  //HAL_Delay(2000);
  //dispense_item1(-19961.189355468749, 5);//works
  //dispense_item1(-19961.189355468749, 5,190);

  /*
  HAL_I2C_Master_Transmit_IT(&hi2c1, 0x01, temp_array, 3);
  HAL_Delay(2000);
*/
  HAL_I2C_Master_Transmit_IT(&hi2c1, 0x01, temp_array, 3);

  /* USER CODE END 2 */
 
 

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */


  while (1)
  {
    /* USER CODE END WHILE */

	  if(Trigger_Flag){
		  HAL_I2C_Master_Transmit_IT(&hi2c1, 0x01, temp_array, 3);
		  Trigger_Flag = 0;
	  }

	  //HAL_I2C_Master_Receive_IT(&hi2c1, 0x01, rx_array, 1);
	  HAL_Delay(2000);
	  //HAL_I2C_Master_Transmit_IT(&hi2c1, 0x15, temp_array, 3); // For the Master. Also set the address to 0 if used
/*
	   //SLAVE MODULE STUFF
	  HAL_I2C_Slave_Receive_IT(&hi2c1, rx_array, 3); // For the Slave
	  if(Process_Flag){
		  process_i2c_instruction(rx_array, TARE_ERROR);
		  rx_array[0]=rx_array[1]=rx_array[2] = Process_Flag = 0;
	  }
*/


    /* USER CODE BEGIN 3 */

	 //in_kg = weight_in_kg(error);
	 //temp_weight = get_data();

	 HAL_Delay(1000);

	  /*
	  //THis is the PWM code
	  in_kg = weight_in_kg(error);
	  temp_weight = get_data();

	  HAL_Delay(300);

	  if(in_kg > 0.4){
		  i = 250;
	  }
	  else if(in_kg > 0.2){
		  i = 50;
	  }
	  else{
		  i = 0;
	  }

	  //THis is the PWM code
	  */
		/*
		if(i == 0){
			factor =  10;
		}
		if(i == 250){
			factor =  -10;
		}

		i += factor;
		*/
		//htim14.Instance->CCR1 = i;//PWM

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL8;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = SLAVE_ADDRESS;//0x15;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */
  hi2c1.Init.OwnAddress1 = 0x6E;
  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 3;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM14 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM14_Init(void)
{

  /* USER CODE BEGIN TIM14_Init 0 */

  /* USER CODE END TIM14_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM14_Init 1 */

  /* USER CODE END TIM14_Init 1 */
  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 32000;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 1000;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 500;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim14, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM14_Init 2 */

  /* USER CODE END TIM14_Init 2 */
  HAL_TIM_MspPostInit(&htim14);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 38400;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */
  //__HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE);
  /* USER CODE END USART1_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : P A0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PA1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */


void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c){
	if(hi2c->Instance == I2C1){
		if(rx_array[0] != 0x00){
			Process_Flag = 1;
		}

	}

}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == USART1){
/*
		if(device_state == STATE_UNADDRESSED){
			if(temp_flag == 0 && rx_array[0] == 'S'){
				temp_flag = 1;
			}
			else if(rx_array[0] == 'K'){
				device_state = STATE_ADDRESSED;
			}
			else if(rx_array[0] != 'K'){
				temp_flag = 0;
			}
		}
		else if(device_state == STATE_ADDRESSED && rx_array[0] == 0x95){
			//break;
			//HAL_TIM_Base_Start_IT(&htim3);
			//HAL_UART_Receive_IT(&huart1, &rx_array[0], 1);
			HAL_UART_Receive_IT(&huart1, rx_array, 1);
		}

		asm("NOP");



		//second
		if(device_state == STATE_UNADDRESSED){
			if(rx_array[0] == START_IDENTIFIER[temp_flag]){
				temp_flag++;
			}
			else if(((DEVICE_ADDRESS >> ((temp_flag - NB_IDENTIFIER)*8)) & 0xFF) == rx_array[0]){
				temp_flag++;
			}
			else if(temp_flag == 5){
				device_state = STATE_ADDRESSED;
				temp_attr = rx_array[0];
				temp_flag++;
			}
		}
		else if(device_state == STATE_ADDRESSED){
			if(temp_flag > 5 && temp_flag <8){
				temp_nob |= (rx_array[0] << 8*(temp_flag - 6));
			}
		}
		*/
		if(device_state == STATE_UNADDRESSED){
/*
			if(rx_array[0] == START_IDENTIFIER[temp_flag] && temp_flag <5){
				temp_flag++;
				HAL_UART_Receive_IT(&huart1, rx_array, 1);
			}
			else if(temp_flag  == 5){
				temp_flag++;
				HAL_UART_Receive_IT(&huart1, rx_array, 1);
			}
			else if(temp_flag == 6){
				HAL_UART_Receive_IT(&huart1, rx_array, (10));
				temp_flag++;
			}
			else if(temp_flag == 7){
				HAL_UART_Receive_IT(&huart1, rx_array, 1);
			}
*/
			if(temp_flag < 5){
				if(rx_array[0] == START_IDENTIFIER[temp_flag]){
					temp_flag++;
				}
				else{
					temp_flag = 0;
				}
			}
			else if(temp_flag < 6){
				device_state = STATE_ADDRESSED;
				struct rx_packet rx_pkt = {0,0,0,0};
				rx_pkt.attribute = rx_array[0];
				temp_flag++;
			}
			HAL_UART_Receive_IT(&huart1, rx_array, 1);
		}
		else if(device_state == STATE_ADDRESSED){
			if(temp_flag < 7){
				rx_pkt.data_length = rx_array[0];
				rx_pkt.data_ptr = rx_pkt.data;
				temp_flag++;
				HAL_UART_Receive_IT(&huart1, rx_pkt.data_ptr, 1);
				rx_pkt.data_length--;
			}
			else if(temp_flag < 64){
				if(rx_pkt.data_length - 1){
					rx_pkt.data_ptr++;
					HAL_UART_Receive_IT(&huart1, rx_pkt.data_ptr, 1);
					rx_pkt.data_length--;
				}
				else{
					HAL_UART_Receive_IT(&huart1, rx_array, 1);
					rx_pkt.ready_to_process_flag = 1;
					temp_flag = 0;
					device_state = STATE_UNADDRESSED;

				}
			}
			else if(temp_flag > 64){
				temp_flag = 0;
				device_state = STATE_UNADDRESSED;
			}
		}
	}
}

/* DMA
void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
  HAL_GPIO_TogglePin (GPIOC, GPIO_PIN_13);  // toggle PC13
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  HAL_UART_Receive_DMA(&huart1, rx_array, 4);
}
*/
/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */
  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
