/*
 * custom_i2c.c
 *
 *  Created on: 24-May-2020
 *      Author: Rahul
 */

#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_i2c.h"
#include "custom_i2c.h"

/*
 * Code from the datasheet
 *
 *
	I2C slave transmitter code example
	uint32_t I2C_InterruptStatus = I2C1->ISR; // Get interrupt status
	// Check address match
	if ((I2C_InterruptStatus & I2C_ISR_ADDR) == I2C_ISR_ADDR){
		I2C1->ICR |= I2C_ICR_ADDRCF; // Clear address match flag

		// Check if transfer direction is read (slave transmitter)
		if ((I2C1->ISR & I2C_ISR_DIR) == I2C_ISR_DIR){
			I2C1->CR1 |= I2C_CR1_TXIE; // Set transmit IT
		}
	}
	else if ((I2C_InterruptStatus & I2C_ISR_TXIS) == I2C_ISR_TXIS){
		I2C1->CR1 &=~ I2C_CR1_TXIE; // Disable transmit IT
		I2C1->TXDR = I2C_BYTE_TO_SEND; // Byte to send
	}

	I2C slave receiver code example

	uint32_t I2C_InterruptStatus = I2C1->ISR; // Get interrupt status
	if ((I2C_InterruptStatus & I2C_ISR_ADDR) == I2C_ISR_ADDR){
		I2C1->ICR |= I2C_ICR_ADDRCF; // Address match event
	}
	else if ((I2C_InterruptStatus & I2C_ISR_RXNE) == I2C_ISR_RXNE){
	// Read receive register, will clear RXNE flag

		if (I2C1->RXDR == I2C_BYTE_TO_SEND){
		//Process
		}
	}

*/
/*

void custom_i2c_transmit(
		I2C_HandleTypeDef *hi2c,
		unsigned char * outputdata,		//Pointer to the output array
		unsigned char no_of_bytes_to_tx,	//Number of bytes to be transmitted
		unsigned char no_of_bytes_to_rx		//Number of bytes to be received in response
		){

	if (hi2c->State == HAL_I2C_STATE_READY){
		hi2c->State       = HAL_I2C_STATE_BUSY_RX;
		hi2c->Mode        = HAL_I2C_MODE_MASTER;
		hi2c->ErrorCode   = HAL_I2C_ERROR_NONE;

		// Prepare transfer parameters

		hi2c->pBuffPtr    = outputdata;
		hi2c->XferCount   = no_of_bytes_to_tx;
		hi2c->XferOptions = I2C_NO_OPTION_FRAME;
		hi2c->XferISR     = i2c_isr;

		// Check Tx empty
		if ((I2C2->ISR & I2C_ISR_TXE) == I2C_ISR_TXE){
			I2C2->TXDR = I2C_BYTE_TO_SEND; // Byte to send
			I2C2->CR2 |= I2C_CR2_START; // Go
		}
	}
}

void custom_i2c_receive(
		I2C_HandleTypeDef *hi2c,
		unsigned char * outputdata,		//Pointer to array to receive data
		unsigned char no_of_bytes_to_rx,	//Number of bytes to be received
		unsigned char no_of_bytes_to_tx		//Number of bytes to be transmitted in response
		){
	if ((I2C2->ISR & I2C_ISR_RXNE) == I2C_ISR_RXNE){
	// Read receive register, will clear RXNE flag
		if (I2C2->RXDR == I2C_BYTE_TO_SEND){
		// Process
		}
	}
}
*/

/*
void blocking_i2c_transmit(unsigned char *outputdata, unsigned char no_bytes_to_tx, unsigned char no_bytes_to_rx){
	while(no_bytes_to_tx){
		if ((I2C2->ISR & I2C_ISR_TXE) == I2C_ISR_TXE){
			I2C2->TXDR = I2C_BYTE_TO_SEND; // Byte to send
			I2C2->CR2 |= I2C_CR2_START; // Go
		}
		no_bytes_to_tx--;
	}
}
*/


void custom_i2c_transmit(
		unsigned char destination_address,
		unsigned char * output_data,
		unsigned char no_of_tx_bytes,
		unsigned char * input_data,
		unsigned char no_of_rx_bytes
		){

	unsigned char i = 0;

	/* First set the value of AUTOEND in CR2
	 * From page 581 of STM32 F0 series reference manual
	 * This bit is set and cleared by software.
	 * 0: software end mode: TC flag is set when NBYTES data are transferred, stretching SCL low.
	 * 1: Automatic end mode: a STOP condition is automatically sent when NBYTES data are transferred.
	 * Note: This bit has no effect in slave mode or when the RELOAD bit is set.
	 */
	if(no_of_rx_bytes){
		I2C1->CR2 = (0 << CR2_AUTOEND);	//no AUTOEND because there are bytes to be Rx post Tx
	}
	else{
		I2C1->CR2 = (1 << CR2_AUTOEND);	//AUTOEND because there are no bytes to be Rx post Tx
	}

	/*
	 *  Second to set the Slave address
	 */
	I2C1->CR2 = (destination_address << 1);

	/*
	 * Writing the data direction
	 * 0: Master requests a write transfer.
	 * 1: Master requests a read transfer.
	 * Note: Changing this bit when the START bit is set is not allowed.
	 */
	I2C1->CR2 = (0 << CR2_RD_WRN);	//First write the data direction

	/*
	 * Write the Start condition
	 * This bit is set by software, and cleared by hardware after the Start followed by the address
	 * sequence is sent, by an arbitration loss, by a timeout error detection, or when PE = 0. It can
	 * also be cleared by software by writing ‘1’ to the ADDRCF bit in the I2C_ICR register.
	 * 0: No Start generation.
	 * 1: Restart/Start generation:
	 – If the I2C is already in master mode with AUTOEND = 0, setting this bit generates a
	   Repeated Start condition when RELOAD=0, after the end of the NBYTES transfer.
	 – Otherwise setting this bit will generate a START condition once the bus is free.
	 * Note: Writing ‘0’ to this bit has no effect.
	 * The START bit can be set even if the bus is BUSY or I2C is in slave mode.
	 * This bit has no effect when RELOAD is set. In 10-bit addressing mode, if a NACK is
	 * received on the first part of the address, the START bit is not cleared by hardware and
	 * the master will resend the address sequence, unless the START bit is cleared by
	 * software
	 */
 	I2C1->CR2 = (1 << CR2_START);

 	for(i = 0; i<no_of_tx_bytes; i++){
 		while(!(I2C1->ISR & 0x02));
 		if(I2C1->ISR & 0x02){// if I2C_ISR.TXIS = 1 TODO change this to interrupt
 			I2C1->TXDR = &output_data;
 			output_data++;
 		}
 	}

 	while((I2C1->ISR & 0x60)){
 		//To find if TC is 1 or not
 	}



}
