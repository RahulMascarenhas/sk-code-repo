#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal.h"
#include "custom_uart.h"



unsigned char Glb_Bit_Flag = INACTIVE;
unsigned char Glb_Last_Transition = INACTIVE;
unsigned char Glb_Buffer = 0;

void uart_transmit_8b_isr(UART_HandleTypeDef *huart){
	asm("NOP");
	if(huart->gState == HAL_UART_STATE_BUSY_TX){
			if (huart->TxXferCount == 0U)
			{
			  /* Disable the UART Transmit Data Register Empty Interrupt */
			  CLEAR_BIT(huart->Instance->CR1, USART_CR1_TXEIE);

			  /* Enable the UART Transmit Complete Interrupt */
			  SET_BIT(huart->Instance->CR1, USART_CR1_TCIE);
			}
			else
			{
			  huart->Instance->TDR = (uint8_t)(*huart->pTxBuffPtr & (uint8_t)0xFF);
			  huart->pTxBuffPtr++;
			  huart->TxXferCount--;
			}
		}
}

HAL_StatusTypeDef uart_transmit_8b(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size){
	/*
	 *A much simpler transmit function that works much faster than the inbuilt function and is easier to read/debug.
	 *Takes 3 parameters:
	 *huart: is the UART interface number that is being used
	 *pData: is the pointer for the Data to be output
	 *Size: number of bytes to be transmitted
	 */

	 uint8_t  *pdata8bits;
	 uint32_t tickstart;

	 //tickstart = HAL_GetTick();

	 pdata8bits  = pData;
	 huart->pTxBuffPtr = pData;
	 huart->TxXferSize  = Size;
	 huart->TxXferCount = Size;
	 huart->TxISR       = NULL;

	if (huart->gState == HAL_UART_STATE_READY){
		 /*
		 while (huart->TxXferCount > 0U){//blocking code
			//huart->Instance->TDR = (uint8_t)(*pdata8bits & 0xFFU);
			huart->Instance->TDR = *pdata8bits;
			pdata8bits++;
			//huart->TxXferCount--;
		 }
		 huart->gState = HAL_UART_STATE_READY;
		*/
		//UART_TxISR_8BIT(&huart);
		huart->gState = HAL_UART_STATE_BUSY_TX;
		huart->TxISR = uart_transmit_8b_isr;
		SET_BIT(huart->Instance->CR1, USART_CR1_TXEIE);
		 return HAL_OK;
	}
	else
	{
	  return HAL_BUSY;
	}
}


void uart_receive_8b_isr(){
	asm("NOP");
}

HAL_StatusTypeDef uart_receive_8b(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size){
	if (huart->RxState == HAL_UART_STATE_READY){
	    __HAL_LOCK(huart);

	    huart->pRxBuffPtr  = pData;
	    huart->RxXferSize  = Size;
	    huart->RxXferCount = Size;
	    huart->RxISR       = NULL;

	    /* Computation of UART mask to apply to RDR register */
	    UART_MASK_COMPUTATION(huart);

	    huart->ErrorCode = HAL_UART_ERROR_NONE;
	    huart->RxState = HAL_UART_STATE_BUSY_RX;

	    /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
	    SET_BIT(huart->Instance->CR3, USART_CR3_EIE);

	    //huart->RxISR =;

	    __HAL_UNLOCK(huart);

		/* Enable the UART Parity Error interrupt and Data Register Not Empty interrupt */
		SET_BIT(huart->Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE);

		return HAL_OK;
	  }
	else{
		return HAL_BUSY;
	}
}


//DMA

//DMA


//Stuff for bus arbitration for Slave responses

void process_ext_isr(UART_HandleTypeDef *huart, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin){
	if(huart->gState == HAL_UART_STATE_READY){
		//Not transmitting any data but detects a change in the line
		if(HAL_GPIO_ReadPin(GPIOx, GPIO_Pin)){

		}

	}
	else if(huart->gState == HAL_UART_STATE_BUSY_TX){

	}
}
