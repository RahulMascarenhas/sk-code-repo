/*
 * custom_bus.c
 *
 *  Created on: 18-Mar-2020
 *      Author: Rahul
 */


#include "custom_bus.h"



void form_output_packet(unsigned char packet_type,
						unsigned char input_data[]){
	unsigned char output_data[NB_MAX_DATA_LEN];
	unsigned int output_data_length;
	unsigned char i;
	unsigned input_data_length;

	for(i = 0; i < NB_MAX_DATA_LEN; i++){
		output_data[i] = 0xFF;
	}


	input_data_length = sizeof(input_data)/sizeof(input_data[0]);

	//Identifier
	output_data[0] = 'S';
	output_data[1] = 'K';

	output_data_length = NB_IDENTIFIER;

	//Address
	for(i = 0; i < NB_ADDRESS; i++){
		output_data[output_data_length] = (DEVICE_ADDRESS >> (i*8)) & 0xFF;
		output_data_length++;
	}

	//Attributes
	for(i = 0; i < NB_ATTRIBUTES; i++){
		output_data[output_data_length] = (packet_type >> (i*8)) & 0xFF;
		output_data_length++;
	}

	//No of data bytes
	for(i = 0; i < NB_DATA_LENGTH; i++){
		output_data[output_data_length] = (input_data_length >> (i*8)) & 0xFF;
		output_data_length++;
	}

	//Data
	for(i = 0; i < input_data_length-1; i++){
		output_data[output_data_length] = input_data[i];
		output_data_length++;
	}

	return output_data;
}



unsigned char form_output_packet1(
		unsigned char packet_type,
		unsigned char input_data[],
		unsigned char *output_ptr,
		unsigned int device_address
		){

	unsigned int output_data_length;
	unsigned char i;
	unsigned char input_data_length; //Make sure to change to type int this if sending more than 256 bytes of data

	input_data_length = sizeof(input_data)/sizeof(input_data[0]);

	//Identifier
	*output_ptr = 'S';
	*(output_ptr+1) = 'K';

	output_data_length = NB_IDENTIFIER;

	//
	//Address
	for(i = 0; i < NB_ADDRESS; i++){
			*(output_ptr + output_data_length) = (device_address >> (i*8)) & 0xFF;
			output_data_length++;
		}
	/*
	for(i = 0; i < NB_ADDRESS; i++){
		*(output_ptr + output_data_length) = (DEVICE_ADDRESS >> (i*8)) & 0xFF;
		output_data_length++;
	}
	*/

	//Attributes
	for(i = 0; i < NB_ATTRIBUTES; i++){
		*(output_ptr + output_data_length) = (packet_type >> (i*8)) & 0xFF;
		output_data_length++;
	}

	//No of data bytes
	for(i = 0; i < NB_DATA_LENGTH; i++){
		*(output_ptr + output_data_length) = (input_data_length >> (i*8)) & 0xFF;
		output_data_length++;
	}

	//Data
	for(i = 0; i < input_data_length-1; i++){
		*(output_ptr + output_data_length) = input_data[i];
		output_data_length++;
	}

	//

	return output_data_length;

}

