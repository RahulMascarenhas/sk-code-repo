/*
 * custom_bus.h
 *
 *  Created on: Mar 12, 2020
 *      Author: Rahul
 */

#ifndef INC_CUSTOM_BUS_H_
#define INC_CUSTOM_BUS_H_


/*
 * The bus protocol has the following format
 *
 * IDENTIFIER + DESTINATION ADDRESS + ATTRIBUTES + DATA LENGTH + DATA + CRC
 *
 * The IDENTIFER are the ASCII characters 'S' and 'K'
 * DESTINATION ADDRESS is the destination address for the packet
 * ATTRIBUTES describe whether the packet is an instruction or response packet
 * DATA LENGTH is only the number of bytes of the DATA field
 * DATA is the data (in the case of a response) or should be empty in the case of an instruction
 * CRC for error handling
 *
 */

#define MASTER_MODE			0
#define SLAVE_MODE			1
#define DEVICE_MODE			SLAVE_MODE	//This is the default //TODO must put this field somewhere else

// #define DEVICE_ADDRESS		0xFEEDE4	//TODO must put this field somewhere else
#define DEVICE_ADDRESS		0xFEEDE5	//TODO must put this field somewhere else

#define STATE_ADDRESSED		1
#define STATE_UNADDRESSED	2

//const unsigned char START_IDENTIFIER[] = {'S', 'K'};


//First define how many bytes each field has
#define NB_IDENTIFIER		2
#define NB_ADDRESS			3
#define NB_ATTRIBUTES		1
#define NB_DATA_LENGTH		1
#define NB_CRC				2
#define NB_OVERHEAD			(NB_IDENTIFIER+NB_ADDRESS+NB_ATTRIBUTES+NB_DATA_LENGTH+NB_CRC)	//total overhead that is not the actual data
#define NB_MAX_DATA_LEN		64

//Packet type response or instruction
#define PT_INSTR			0
#define PT_RESPONSE			1

struct {
	unsigned int packet_type:1;	//0: INSTRUCTION ; 1:RESPONSE
	unsigned int unassigned:7;
}attr_type;

struct data_packet{
	unsigned int 		identifier[NB_IDENTIFIER];
	unsigned int 		address[NB_ADDRESS];
	//struct attr_type 	attributes;
	unsigned int 		data_length[NB_DATA_LENGTH];
	unsigned int 		data[NB_MAX_DATA_LEN];
	unsigned int 		crc[NB_CRC];
};

struct rx_packet{
	unsigned char 		attribute;
	unsigned char 		data_length;
	unsigned char *		data_ptr;
	unsigned char 		data[NB_MAX_DATA_LEN];
	unsigned char 		ready_to_process_flag;
};

void form_output_packet(unsigned char packet_type, unsigned char data[]);


unsigned char form_output_packet1(
		unsigned char packet_type,
		unsigned char input_data[],
		unsigned char *output_ptr,
		unsigned int device_address);

#endif /* INC_CUSTOM_BUS_H_ */
