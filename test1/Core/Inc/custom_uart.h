/*
 * custom_uart.h
 *
 *  Created on: 16-Mar-2020
 *      Author: tarun
 */

#ifndef INC_CUSTOM_UART_H_
#define INC_CUSTOM_UART_H_

//DMA
#define DMA_RX_BUFFER_SIZE          64
uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];

#define UART_BUFFER_SIZE            256
uint8_t UART_Buffer[UART_BUFFER_SIZE];

#define INACTIVE 0
#define ACTIVE	 1

unsigned char Glb_Bit_Flag;
unsigned char Glb_Last_Transition;
unsigned char Glb_Buffer;
//DMA


HAL_StatusTypeDef uart_transmit_8b(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size);
void uart_transmit_8b_isr(UART_HandleTypeDef *huart);

void process_ext_isr(UART_HandleTypeDef *huart, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);


#endif /* INC_CUSTOM_UART_H_ */
