/*
 * custom_i2c.h
 *
 *  Created on: 26-May-2020
 *      Author: tarun
 */

#ifndef INC_CUSTOM_I2C_H_
#define INC_CUSTOM_I2C_H_

//I2C_CR1
// 0: Disable, 1: Enable
#define CR1_SBC			16	//Bit 16 SBC: Slave byte control
#define CR1_TCIE		06	//Bit 6 TCIE: Transfer Complete interrupt enable
#define CR1_STOPIE		05	//Bit 5 STOPIE: STOP detection Interrupt enable
#define CR1_NACKF		04	//Bit 4 NACKIE: Not acknowledge received Interrupt enable
#define CR1_ADDRIE		03	//Bit 3 ADDRIE: Address match Interrupt enable (slave only)
#define CR1_RXIE		02	//Bit 2 RXIE: RX Interrupt enable
#define CR1_TXIE		01	//Bit 1 TXIE: TX Interrupt enable
#define CR1_PE			00	//Bit 0 PE: Peripheral enable


//I2C_CR2
#define CR2_AUTOEND		25	//Bit 25 AUTOEND: Automatic end mode (master mode)
#define CR2_RELOAD		24	//Bit 24 RELOAD: NBYTES reload mode
#define CR2_NBYTES		23	//Bits 23:16 NBYTES[7:0]: Number of bytes
#define CR2_NACK		15	//Bit 15 NACK: NACK generation (slave mode)
#define CR2_STOP		14	//Bit 14 STOP: Stop generation (master mode)
#define CR2_START		13	//Bit 13 START: Start generation
#define CR2_RD_WRN		10	//Bit 10 RD_WRN: Transfer direction (master mode)
#define CR2_SADD_7_1	7	//Bits 7:1 SADD[7:1]: Slave address bit 7:1 (master mode)
#define CR2_SADD_0		0	//


void custom_i2c_transmit1(
		I2C_HandleTypeDef *hi2c,
		unsigned char * outputdata,		//Pointer to the output array
		unsigned char no_of_bytes_to_tx,	//Number of bytes to be transmitted
		unsigned char no_of_bytes_to_rx		//Number of bytes to be received in response
		);

void custom_i2c_receive(
		I2C_HandleTypeDef *hi2c,
		unsigned char * outputdata,		//Pointer to array to receive data
		unsigned char no_of_bytes_to_rx,	//Number of bytes to be received
		unsigned char no_of_bytes_to_tx		//Number of bytes to be transmitted in response
		);

void i2c_isr();

void custom_i2c_transmit(
		unsigned char destination_address,
		unsigned char * output_data,
		unsigned char no_of_tx_bytes,
		unsigned char * input_data,
		unsigned char no_of_rx_bytes
		);

#endif /* INC_CUSTOM_I2C_H_ */
