/*
 * global_var.h
 *
 *  Created on: Mar 5, 2020
 *      Author: tarun
 */

#ifndef INC_GLOBAL_VAR_H_
#define INC_GLOBAL_VAR_H_

//Weight sensor stuff

#define WS_STATUS_START		0
#define WS_STATUS_RUNNING	1
#define WS_STATUS_END		2

int WS_Flag;
int WS_Pulse;

int WS_Count;
int WS_Result ;

#endif /* INC_GLOBAL_VAR_H_ */
